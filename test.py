import unittest
import numpy as np
import mandelbrot as mb


class MandelbrotTest(unittest.TestCase):
    def setUp(self):
        # This will contain a list of the methods we will test
        self.methods = [
            mb.mandelbrot_set_numpy,
            mb.mandelbrot_set_numba,
            mb.mandelbrot_set_cython,
            mb.mandelbrot_set_parallel,
        ]

        self.real = {"min": -2, "max": 1, "p": 200}
        self.imag = {"min": -1.5, "max": 1.5, "p": 200}

        self.I = 200
        self.T = 10

        self.M0 = mb.mandelbrot_set_numpy(self.real, self.imag, self.I, self.T)

    def test_mandelbrot(self):
        for m in self.methods:
            MUT = m(self.real, self.imag, self.I, self.T)

            comparison = np.allclose(MUT, self.M0)

            print("{}: {}".format(m, comparison))

            self.assertTrue(comparison)


if __name__ == "__main__":
    unittest.main()
