import pandas as pd
import matplotlib.pyplot as plt


def benchmark_plot(filename):
    data = pd.read_csv(filename, index_col=0)
    fig, ax = plt.subplots(figsize=(12, 4))

    data.plot(ax=ax)
    ax.set_ylabel("Computation time [s]")

    return fig, ax


if __name__ == "__main__":
    for file in ["benchmark_parallel.csv", "benchmark.csv"]:
        try:
            fig, ax = benchmark_plot(file)
            fig.savefig(file[:-4] + ".png")
        except:
            pass
