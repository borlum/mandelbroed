# Mandelbrot

## Example
![Mandelbrot plot](output/mandelbrot.png "mandelbrot")

## Structure
The software is structured in 4 main modules:

- Mandelbrot implementations (`mandelbrot.py`)
- Testing (`test.py`)
- Benchmarking (`benchmark.py`)
- Plotting (`display.py`)

## Running the software

### Requirements
The requiremensts are given in `requirements.txt`, and can be installed using `pip` by executing:

```
    pip install -r requirements.txt
```

### Compilation step
The **Cython-version** of the implementation needs to be compiled, which is done by issuing:

```
    python setup.py build_ext --inplace
```

Which executes a compilation step, and install the **Cython-version** as a package, to be accessed by importing `mandelbrotc`.

### Benchmarking
Two differents benchmarks are conducted:

1. Comparing the computation time for the different implementations, over varying sizes of the complex plane considered (varying *p*).

2. Comparing computation time of the parallel implementation, for varying sizes of the complex plane considered (varying *p*) and for varying number of computation units.

The first benchmark is run, by running `benchmark.py` as a script. The output is saved as `benchmark.csv`. The second benchmark is run, by running `benchmark_parallel.py` and the output is saved as `benchmark_parallel.csv`.

The results can be plotted using `benchmark_plot.py`.

### Tests
Tests can be run by running `python test.py`. The test evaluates the correctness by comparing all implementations with the naïve implementation, using `numpy.allclose`. If all implementations give the same result, the test passes.

### Mandelbrot plots
To generate a plot of a Mandelbrot-set, run `python display.py`, which will generate plots using the fastests method, saving it to `output/mandelbrot.pdf`.

## Mandelbrot implementations
Four different implementations are considered:

1. Numpy
2. Cython
3. Numba
4. Parallel

### Prototype
The prototype for all the implementations, is:

```
    def mandelbrot_set(real, imag, I, T):
        ...
```

The `real` and `imag` arguments are `dictionaries`, specifying the complex-plane of interest, e.g.:


```
    real = {"min": -2, "max": 1, "p": 2000}
    imag = {"min": -1.5, "max": 1.5, "p": 2000}
```

The `I` and `T` arguments are iteration limit and saturation limit for the mandelbrot-algorithm.

The return value is always the computed mandelbrot set, *M*, as a `numpy.array` (matrix).

### Numpy (naïve-version)
The **numpy-version** is the naïve version; it is a naïve implementation but using numpy arrays, and no care given to specific vectorization. The implementation is divided into two methods:

1. Calculating a point
2. Calculating the entire set

The point calculation is implemented as:

```
    def mandelbrot_point(z, I, T=10):
        c = z

        for i in range(I):
            if np.abs(z) > T:
                return (i + 1) / I
            z = z * z + c
        return 1
```

The set calculation is implemented as:

```
    def mandelbrot_set(real, imag, I, T, iterator):
        R = np.linspace(real["min"], real["max"], real["p"])
        J = np.linspace(imag["min"], imag["max"], imag["p"])

        M = np.empty((real["p"], imag["p"]))
        for i in range(real["p"]):
            for j in range(imag["p"]):
                M[i, j] = iterator(R[i] + 1j * J[j], I, T)

        return M
```

The set calculation iterates over all points to be calculated, and calls the point calculation, through the callback, `iterator`. Thus the set calculation can use an arbitrary point calculation, passed to it, allowing for more diverse implementations. The specific **numpy-version** is instantiated in the namespace of the module as:

```
    def mandelbrot_set_numpy(real, imag, I, T):
        return mandelbrot_set(real, imag, I, T, mandelbrot_point)
```

### Cython
The **cython-version** has been implemented, by copying the **numpy-version** to `mandelbrot.pyx`, and setting up a Cython compilation step in `setup.py`. It is instantiated in the mandelbrot namespace by:

```
    from mandelbrotc import mandelbrot_set_cython
```

### Numba
The **numba-version**, being the simplest optimization realization, is implemented by importing `jit` from Numba, and using the `jit` decorator on the **numpy** point calculation:

```
    def mandelbrot_set_numba(real, imag, I, T):
        mandelbrot_point_numba = jit(mandelbrot_point)
        return mandelbrot_set(real, imag, I, T, mandelbrot_point_numba)
```
As such, we only use Numba to optimize the point calculation -- and do not attempt any optimization on the iteration over all points.

### Parallel
The **parallel-version** uses `multiprocessing`, to instantiate a `Pool` and let *N* computation units calculate points in parallel. The parallel version calculates the complex-plane for the region of interest:

```
    R = np.linspace(real["min"], real["max"], real["p"])
    J = np.linspace(imag["min"], imag["max"], imag["p"])

    C = R[:, None] + 1j * J[None, :]
```

and then flattens the *C* matrix, to serialize the data. We then form a `list` of `tuples`, containing the relevant point calculation arguments, and use `Pool.map` to divide-and-conquer:

```
    C_flat = C.flatten()
    n_args = [(z, I, T) for z in C_flat]
    results = pool.map(_point, C_flat)
    M_flat = np.array(list(results))

    return M_flat.reshape(real["p"], imag["p"])
```

Notice that we call `_point` instead of the previously defined `mandelbrot_point(...)`. Here, `_point` is a wrapper function, which unpacks arguments and then calls `mandelbrot_point(...)`.

**Maybe the parallel version could be further optimized, by grouping the data into chunks, to avoid too much overhead.**

## Benchmarking results

## Benchmarking the different implementations
Benchmarking the different implementations was done on a laptop with a Intel(R) Core(TM) i7-6600U CPU @ 2.60GHz. The results are presented here:

![Benchmarking different implementations of mandelbrot.](output/benchmark.png "Benchmark 1")

It is clear, that the **numba-version** outperforms all other implementations; the simplest optimization step providing the biggest gain in performance. It should be noted, that the **Cython-version** could improve further, by providing the compiler with more information, which is also evident from the Cython HTML report, generated using `cython -a mandelbrot.pyx`, which is shown here:

![Cython report, showing that there is room for improvement.](output/mandelbrot_cython_report.png "Cython report")

## Benchmarking the parallel-version
Benchmarking the **parallel-version** was done on `js1.es.aau.dk`, increasing the number of computational units from 1 to 10, in steps of 1. The results are shown here:

![Benchmarking the parallel version.](output/benchmark_parallel.png "Benchmark 2")

We now instead consider Amdahls law, and plot the speedup vs. number of computation units, *N*:

![Speedup vs. number of computation units.](output/parallel_speedup.png "Benchmark 2")

The payoff is to a large extent linear, but the slope is decreasing, and had we included more cores, we would likely start seeing that it did not make sense to keep throwing cores at the problem.

## Testing and Benchmarking (software architecture)
Given that all implementation are available in `mandelbrot.py`, and given the fact that all Python functions are in fact objects, we use the following approach, in importing and calling the implementations we are interested in:

```
    import mandelbrot as mb

    implementations = [
        mb.mandelbrot_set_numpy,
        mb.mandelbrot_set_numba,
        mb.mandelbrot_set_cython,
        mb.mandelbrot_set_parallel,
    ]

    for impl in implementations:
        result = impl(<common arguments for calling>)
```



