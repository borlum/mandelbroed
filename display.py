import numpy as np
import mandelbrot as mb
import matplotlib.pyplot as plt


def mandelbrot_plot(M, real, imag):
    d = 10
    dpi = real["p"] / d

    fig, ax = plt.subplots(figsize=(d, d), dpi=dpi)

    ax.set_title("Mandelbrot: {} x {}".format(real["p"], imag["p"]))
    ax.set_ylabel("$\mathcal{J}$")
    ax.set_xlabel("$\mathcal{R}$")

    ax.imshow(M.T, origin="lower", cmap=plt.cm.gist_heat)

    return fig, ax


if __name__ == "__main__":
    real = {"min": -2, "max": 1, "p": 5000}
    imag = {"min": -1.5, "max": 1.5, "p": 5000}
    I = 100
    T = 10

    M = mb.mandelbrot_set_numba(real, imag, I, T)

    fig, ax = mandelbrot_plot(M, real, imag)

    fig.savefig("output/mandelbrot.pdf", bbox_inches="tight")
