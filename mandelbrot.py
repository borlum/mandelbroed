import os
import numpy as np

from numba import jit
from multiprocessing import Pool

# from concurrent.futures import ThreadPoolExecutor
from mandelbrotc import mandelbrot_set_cython


def mandelbrot_point(z, I, T=10):
    c = z

    for i in range(I):
        if np.abs(z) > T:
            return (i + 1) / I
        z = z * z + c
    return 1


def _point(args):
    z, I, T = args
    return mandelbrot_point(z, I, T)


def mandelbrot_set(real, imag, I, T, iterator):
    R = np.linspace(real["min"], real["max"], real["p"])
    J = np.linspace(imag["min"], imag["max"], imag["p"])

    M = np.empty((real["p"], imag["p"]))
    for i in range(real["p"]):
        for j in range(imag["p"]):
            M[i, j] = iterator(R[i] + 1j * J[j], I, T)

    return M


def mandelbrot_set_numpy(real, imag, I, T):
    return mandelbrot_set(real, imag, I, T, mandelbrot_point)


def mandelbrot_set_numba(real, imag, I, T):
    mandelbrot_point_numba = jit(mandelbrot_point)
    return mandelbrot_set(real, imag, I, T, mandelbrot_point_numba)


def mandelbrot_set_parallel(real, imag, I, T, N_pu=2):
    pool = Pool(processes=N_pu)

    R = np.linspace(real["min"], real["max"], real["p"])
    J = np.linspace(imag["min"], imag["max"], imag["p"])

    C = R[:, None] + 1j * J[None, :]

    C_flat = C.flatten()
    n_args = [(z, I, T) for z in C_flat]
    results = pool.map(_point, n_args)
    M_flat = np.array(list(results))

    return M_flat.reshape(real["p"], imag["p"])


if __name__ == "__main__":
    real = {"min": -2, "max": 1, "p": 20}
    imag = {"min": -1.5, "max": 1.5, "p": 20}
    I = 100
    T = 10
    M_x = mandelbrot_set_numpy(real, imag, I, T)
    print(M_x)
