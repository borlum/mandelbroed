import mandelbrot as mb
import benchmark as bm
from functools import partial
import pandas as pd

# Make
methods = []
for N in range(1, 11):
    m = partial(mb.mandelbrot_set_parallel, N_pu=N)
    m.__name__ = "mandelbrot_set_parallel_{}".format(N)
    methods.append(m)

b = bm.Benchmark(methods)

times = []
for p in range(250, 2750, 250):
    print("Calculating for p = {}...".format(p))
    b.real["p"] = p
    b.imag["p"] = p

    results = b.run()
    results["p"] = p
    times.append(results)

    # Save and print as we go
    results = pd.DataFrame(times)
    results.set_index("p", inplace=True)
    results.to_csv("benchmark_parallel.csv")
    print(results)
