import numpy as np

def mandelbrot_point(z, I, T=10):
    c = z
    for i in range(I):
        if abs(z) > T:
            return (i + 1)/I
        z = z*z + c
    return 1

def mandelbrot_set_cython(real, imag, I, T):
    R = np.linspace(real["min"], real["max"], real["p"])
    J = np.linspace(imag["min"], imag["max"], imag["p"])
    
    M = np.empty((real["p"], imag["p"]))
    for i in range(real["p"]):
        for j in range(imag["p"]):
            M[i,j] = mandelbrot_point(R[i] + 1j*J[j], I, T)
    
    return M