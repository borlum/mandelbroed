import time
import mandelbrot as mb


class Benchmark:
    def __init__(self, methods, I=100, T=10, p=200):
        self.real = {"min": -2, "max": 1, "p": p}
        self.imag = {"min": -1.5, "max": 1.5, "p": p}
        self.I = I
        self.T = T

        self.methods = methods

    def run(self):
        times = {}

        for method in self.methods:
            t0 = time.time()
            M = method(self.real, self.imag, self.I, self.T)
            dt = time.time() - t0

            times[method.__name__] = dt

        return times


def benchmark_plot():
    data = pd.read_csv("benchmark.csv", index_col=0)
    data.plot()
    plt.show()


if __name__ == "__main__":
    import pandas as pd

    def mandelbrot_set_parallel_2(real, imag, I, T):
        return mb.mandelbrot_set_parallel(real, imag, I, T, 2)

    def mandelbrot_set_parallel_4(real, imag, I, T):
        return mb.mandelbrot_set_parallel(real, imag, I, T, 4)

    b = Benchmark(
        [
            mb.mandelbrot_set_numpy,
            mb.mandelbrot_set_numba,
            mb.mandelbrot_set_cython,
            mandelbrot_set_parallel_2,
            # mandelbrot_set_parallel_4,
        ]
    )

    times = []

    for p in range(250, 2750, 250):
        print("Calculating for p = {}...".format(p))
        b.real["p"] = p
        b.imag["p"] = p

        results = b.run()
        results["p"] = p
        times.append(results)

    results = pd.DataFrame(times)
    results.set_index("p", inplace=True)

    results.to_csv("benchmark.csv")

    print(results)
